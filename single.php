<?php get_header(); ?>

<div class="two-column-container">

	<div class="column-two-thirds">
		<?php if (function_exists('qt_custom_breadcrumbs')) qt_custom_breadcrumbs(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

		<header>
    	<h1><?php the_title(); ?></h1>
    	<?php include (TEMPLATEPATH . '/includes/meta.php' );?>
  	</header>

  	<?php	the_content(); ?>

		<footer>
			<?php wp_link_pages(array('before' => 'Pages: ', 'next_or_number' => 'number')); ?>
			<?php the_tags( 'Tags: ', ', ', ''); ?>

			<div id="post-author">
				<?php if(function_exists('get_avatar')) { echo get_avatar( get_the_author_meta('email'), '58' ); /* This avatar is the user's gravatar (http://gravatar.com) based on their administrative email address */  } ?>

				<?php $shortlink = wp_get_shortlink(); ?>

				<div id="author-description">
					<p>Written by <?php the_author_posts_link() ?><br />
					<a class="share_twitter" title="Tweet This" target="_blank" href="http://twitter.com/home?status=<?php echo str_replace("|", "", get_the_title()); echo(' '); echo $shortlink; ?>"><i class="fa fa-twitter-square"></i> <?php _e('Tweet This','lesterlaw'); ?></a>

					<a class="share_facebook" title="Share This" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php the_permalink(); ?>&t=<?php the_title(); ?>" target="_blank"><i class="fa fa-facebook-square"></i> <?php _e(' Share This','lesterlaw'); ?></a></p>

				</div>
			</div><!--#post-author-->
		</footer>
	</article>

	<?php include('includes/post-nav.php'); ?>

	<section id="comments">
		<?php comments_template( '', true ); ?>
	</section>

	<?php endwhile; endif; ?>
	</div><!--/.column-two-thirds-->

	<?php get_sidebar()?>

</div><!--/.two-column-container-->

<?php get_footer(); ?>
