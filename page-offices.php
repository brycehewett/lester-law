<?php
/*
Template Name: Offices Template
*/
?>
<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <article class="single-column-container">

    <header>
      <?php if (function_exists('qt_custom_breadcrumbs')) qt_custom_breadcrumbs();?>
      <h1><?php the_title(); ?></h1>
      <?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>
    </header>

    <?php the_content(); ?>
    <?php endwhile; endif; ?>

    <a href="/offices/chattanooga-tennesse/" class="office-location chattanooga">
      <span class='office-link-overlay'>
        <img src="<?php echo get_template_directory_uri(); ?>/images/tennessee-icon.svg"/>
        <span>Chattanooga, Tennessee <i class="fa fa-chevron-circle-right"></i></span>
      </span>
    </a>

    <a href="/offices/cleveland-tennesse/" class="office-location cleveland">
      <span class='office-link-overlay'>
        <img src="<?php echo get_template_directory_uri(); ?>/images/tennessee-icon.svg"/>
        <span>Cleveland, Tennessee <i class="fa fa-chevron-circle-right"></i></span>
      </span>
    </a>
  </article>

<?php get_footer(); ?>
