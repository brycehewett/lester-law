<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>

	<meta charset="<?php bloginfo('charset'); ?>">

	<!-- Always force latest IE rendering engine (even in intranet) & Chrome Frame -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

	<?php if (is_search()) { ?>
	<meta name="robots" content="noindex, nofollow" />
	<?php } ?>

<!--title-->
<title><?php
$page_title = get_post_meta($post->ID, 'page_title', true);
$archive_title = trim(wp_title('', false));

// Page Title for home page
if (is_home()) {bloginfo('name'); echo ' - '; bloginfo('description');}
elseif(is_archive()) { echo bloginfo('name') . ' - Blog archive for '. $archive_title ;}
// If the page title attribute isn't blank then output this
elseif($page_title != '') {bloginfo('name'); echo ' - '; echo strip_tags($page_title);}
else { echo strip_tags(bloginfo('name') . ' - ' . get_the_title());}
?></title>
<!--/title-->

<!--description-->
	<meta name="title" content="<?php
      if (is_home()) { bloginfo('name'); echo ' - '; bloginfo('description'); }
      elseif (is_archive()) { bloginfo('name'); echo ' - Blog Archive for ' . $archive_title; }
      elseif (function_exists('is_tag') && is_tag()) {single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
      elseif (is_search()) {echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
      elseif (!(is_404()) && (is_single()) || (is_page())) {bloginfo('name'); echo ' - ' . $page_title; }
      elseif (is_404()) {bloginfo('name'); echo ' - ' . '404 Not Found'; }
      else {bloginfo('name'); }
        if ($paged>1) { echo ' - page '. $paged; }
  ?>">

	<meta name="author" content="<?php bloginfo('name'); ?>">
	<meta name="Copyright" content="&copy; Copyright <?php bloginfo('name'); echo " " . date("Y"); ?>. All Rights Reserved.">

	<!--  Mobile Viewport meta tag
	j.mp/mobileviewport & davidbcalhoun.com/2010/viewport-metatag
	device-width : Occupy full width of the screen in its current orientation
	initial-scale = 1.0 retains dimensions instead of zooming out if page height > device height
	maximum-scale = 1.0 retains dimensions instead of zooming in if page width < device width-->
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">


	<?php if ( function_exists( 'ot_get_option' ) ) {
  	$favicon = ot_get_option( 'favicon' );

			if ( ! empty( $favicon ) ) {
  		echo '<link rel="shortcut icon" href="'.$favicon.'">';
  		}

	}?>
	<!-- This is the traditional favicon.
		 - size: 16x16 or 32x32
		 - transparency is OK
		 - see wikipedia for info on browser support: http://mky.be/favicon/ -->

	<?php if ( function_exists( 'ot_get_option' ) ) {
  	$apple_touch_icon = ot_get_option( 'apple_touch_icon' );

  		if ( ! empty( $apple_touch_icon ) ) {
	  	echo '<link rel="apple-touch-icon" href="'.$apple_touch_icon.'">';
  		}

	}?>
	<!-- The is the icon for iOS's Web Clip.
		 - size: 57x57 for older iPhones, 72x72 for iPads, 114x114 for iPhone4's retina display (IMHO, just go ahead and use the biggest one)
		 - To prevent iOS from applying its styles to the icon name it thusly: apple-touch-icon-precomposed.png
		 - Transparency is not recommended (iOS will put a black BG behind the icon) -->

	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />


	<?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

	<?php wp_head(); ?>

	<!--[if lt IE 9]>
		<script src="libs/html5shiv/dist/html5shiv.min.js"></script>
	<![endif]-->

	<?php
		if ( function_exists( 'ot_get_option' ) ) {
			$facebook_app_id = ot_get_option( 'facebook_app_id' );
			$default_img = ot_get_option( 'default_facebook_logo' );
			if (has_post_thumbnail($post->ID)){
				$thumb = get_post_meta($post->ID,'_thumbnail_id',false);
				$thumb = wp_get_attachment_image_src($thumb[0], 'thumbnail', false);
				$thumb = $thumb[0];
			}

		if ( ! empty( $facebook_app_id ) ) {

			echo '<meta property="fb:app_id" content="'.$facebook_app_id.'"/>';

		if ( is_single() || is_page()) { ?>

			<meta property="og:type" content="article" />
			<meta property="og:title" content="<?php single_post_title(''); ?>" />
			<meta property="og:description" content="<?php
				while(have_posts()):the_post();
				$out_excerpt = str_replace(array("\r\n", "\r", "\n"), "", get_the_excerpt());
				echo apply_filters('the_excerpt_rss', $out_excerpt);
				endwhile; 	?>" />
			<meta property="og:url" content="<?php the_permalink(); ?>"/>
			<meta property="og:image" content="<?php if ( $thumb[0] == null ) { echo $default_img; } else { echo $thumb; } ?>" />
			<?php  } else { ?>
				<meta property="og:type" content="article" />
				<meta property="og:title" content="<?php bloginfo('name'); ?>" />
				<meta property="og:url" content="<?php bloginfo('url'); ?>"/>
				<meta property="og:description" content="<?php bloginfo('description'); ?>" />
	    	<meta property="og:image" content="<?php  if ( $thumb[0] == null ) { echo $default_img; } else { echo $thumb; } ?>" />
			<?php	}

		}
  	} ?>

	</head>

	<body <?php body_class();?>>

	<?php include('includes/responsive-menu.php');?>

	<div class="site-wrapper">

	<?php echo '<div id="fb-root"></div><script>(function(d, s, id) {var js, fjs = d.getElementsByTagName(s)[0]; if (d.getElementById(id)) return; js = d.createElement(s); js.id = id; js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId='.$facebook_app_id.'&version=v2.0"; fjs.parentNode.insertBefore(js, fjs); }(document, "script", "facebook-jssdk"));</script>';?>

 		<div class="content container">
			<?php include('includes/navigation.php'); ?>
