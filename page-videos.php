<?php
/*
Template Name: Video Page Template
*/

$videos_custom_post_args = array(
'post_type' => 'videos',
'posts_per_page' => -1,
'order_by' => 'menu-order',
'order' => 'ASC' );

$lastest_video_query_args = array(
  'category_name' => 'videos',
  'posts_per_page' => 1
);

$category_query_args = array(
  'category_name' => 'videos',
  'posts_per_page' => 6,
  'offset' => 1
);

$videos_custom_post = new WP_Query( $videos_custom_post_args );
$lastest_video_query = new WP_Query( $lastest_video_query_args );
$category_query = new WP_Query( $category_query_args );

get_header(); if (have_posts()) : while (have_posts()) : the_post(); ?>

<div class="single-column-container">

  <article>
    <header>
      <?php if (function_exists('qt_custom_breadcrumbs')) qt_custom_breadcrumbs();?>
      <h1><?php the_title(); ?></h1>
      <?php if ( has_post_thumbnail() ) { the_post_thumbnail();} ?>
    </header>

		<?php the_content(); ?>

    <?php endwhile; endif; ?>

    <?php while ( $videos_custom_post->have_posts() ) : $videos_custom_post->the_post();
      $feat_image_url = (has_post_thumbnail() ? wp_get_attachment_url( get_post_thumbnail_id() ) : null);?>

      <div class="video-page-video">
        <?php if ( $feat_image_url) : ?>
          <a href="<?php $permalink ?>" class="video-thumbnail" style="background-image:url(<?php echo $feat_image_url ?>);"></a>
        <?php endif ?>

        <h5><?php the_title() ?></h5>
        <a href="<?php the_permalink() ?>"><?php _e('View','lesterlaw') ?><i class="fa fa-angle-right"></i></a>
      </div>
    <?php endwhile; wp_reset_query(); ?>

	</article>
</div>
<div class="weekly-updates">

  <?php if ( $lastest_video_query->have_posts() ) : while ($lastest_video_query->have_posts()) : $lastest_video_query->the_post();?>

    <div class="weekly-updates__latest">
      <div class="weekly-updates__latest--container">
        <h2><?php _e('Weekly Updates', 'lesterlaw') ?></h2>
        <div class="weekly-updates__latest--video-column">
          <h5><time datetime="<?php the_date(DATE_W3C) ?>" class="updated"><?php the_time('F j, Y')?></time></h5>
          <?php the_excerpt() ?>
        </div><!-- weekly-updates__latest-video-column -->
        <div class="weekly-updates__latest--social-links">
          <h3><?php _e( 'Follow Us On Social Media', 'lesterlaw' ) ?></h3>
          <div class="social-links">
            <a href="https://www.facebook.com/MartinWLesterPA/" target="_blank"><i class="social-links__social-button fa fa-facebook" aria-hidden="true"></i></a>

            <a href="https://www.youtube.com/lesterlaw" target="_blank"><i class="social-links__social-button fa fa-youtube-play" aria-hidden="true"></i></a>

            <a href="https://twitter.com/LesterLawTN" target="_blank"><i class="social-links__social-button fa fa-twitter" aria-hidden="true"></i></a>
          </div>
          <span class="social-links-spacer">OR</span>
          <a href="/contact-us" class="button white"><?php _e('Contact Us', 'lesterlaw') ?></a>
        </div><!--weekly-updates__latest-social-link -->
      </div><!--weekly-updates__latest-container -->
    </div><!--weekly-updates__latest -->

  <?php endwhile; endif; wp_reset_query();?>

  <div class="weekly-updates__previous">
    <?php if ( $category_query->have_posts() ) : while ($category_query->have_posts()) : $category_query->the_post();

      $feat_image_url = (has_post_thumbnail() ? wp_get_attachment_url( get_post_thumbnail_id() ) : null);?>

      <div class="weekly-updates__previous--video">
        <?php if ( $feat_image_url) : ?>
          <a href="<?php the_permalink() ?>" class="video-thumbnail" style="background-image:url(<?php echo $feat_image_url ?>);"></a>
        <?php endif ?>
        <h5><?php the_title() ?></h5>
        <time datetime="<?php the_date(DATE_W3C)?>" class="updated"><?php the_time('F j, Y') ?></time>
      </div>

    <?php endwhile; endif; wp_reset_query(); ?>
  </div> <!--weekly-update__previous-->

  <a href="/category/videos/" class="button red more-videos-button"><?php _e('View More Videos', 'lesterlaw');?></a>
</div><!--weekly-updates-->

<?php get_footer(); ?>
