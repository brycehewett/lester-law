<?php
/*
Template Name: Team Template
*/
?>
<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <article class="single-column-container">

    <header>
      <?php if (function_exists('qt_custom_breadcrumbs')) qt_custom_breadcrumbs();?>

      <h1><?php the_title(); ?></h1>

      <?php if ( has_post_thumbnail() ) { the_post_thumbnail();} ?>

    </header>

		<?php the_content(); ?>

    <?php endwhile; endif; ?>

    <?php $args = array(
    'post_type' => 'team-members',
    'posts_per_page' => -1,
    'order_by' => 'menu-order',
    'order' => 'ASC' );
    $loop = new WP_Query( $args );
    while ( $loop->have_posts() ) : $loop->the_post();

      $name = get_the_title();
      $title = 'member_title';

      echo '<div class="team-member">';
      if ( has_post_thumbnail() ) { the_post_thumbnail();};
      echo '<h2>' . $name . '</h2>';
      echo '<p>' . get_post_meta($post->ID, $title, true) . '</p>';
      echo '<a href="' . get_the_permalink() . '">' . __('Read Bio','lesterlaw') . ' <i class="fa fa-angle-right"></i></a>';
      echo '</div>';

    endwhile; ?>

	</article>

<?php get_footer(); ?>
