<?php get_header();?>

<?php if (function_exists('pll_current_language')) {

  $current_lang = pll_current_language();

  if ($current_lang == 'es') {
    $immigration_law_link = '/es/ley-de-inmigracion/';
    $our_values_link = '/es/por-que-elegir-nuestra-firma/nuestros-valores/';
    $contact_us_link = '/es/contactenos/';
  } else {
    $immigration_law_link = '/immigration-law';
    $our_values_link = '/our-values';
    $contact_us_link = '/contact-us';
  }
}?>

<div class="promo-image">

  <div class="promo-text">
    <h1><?php _e('Responsive. Local. Focused.','lesterlaw'); ?><br />
      <span><?php _e('Why Lester Law is the right choice for immigration law in Chattanooga.','lesterlaw'); ?></span>
    </h1>
    <a href="<?php echo $our_values_link ?>"><?php _e('Learn More','lesterlaw'); ?> &nbsp;<i class="fa fa-chevron-circle-right"></i></a><a class="button white" href="<?php echo $contact_us_link ?>"><?php _e('Contact Us','lesterlaw'); ?></a>
  </div>
</div>

<div class="teaser-text">
  <h2><?php _e('Immigration law is complex and challenging.','lesterlaw'); ?><br />
    <?php _e('It\'s important to choose a law firm that focuses on what you need.','lesterlaw'); ?><br /></h2>
  <h2><strong><?php _e('We focus on immigration law.','lesterlaw'); ?></strong></h2>
</div>

<div class="section i-law">
  <div class="section-img">
  </div>
  <div class="section-text">
    <h3><?php _e('Immigration Law','lesterlaw'); ?></h3>
    <p><?php _e('Our attorneys have dedicated their practices to helping individuals and businesses deal with the difficult U.S. immigration system. Whether you are starting or buying a business, helping a family member immigrate, or even facing possible deportation, chances are we can assist you.','lesterlaw'); ?></p>
    <a href="<?php echo $immigration_law_link ?>"><?php _e('Learn More','lesterlaw'); ?> &nbsp;<i class="fa fa-angle-right"></i></a>
  </div>
</div>

<?php $args = array(
'post_type' => 'videos',
'posts_per_page' => -1,
'order_by' => 'menu-order',
'order' => 'ASC' );
query_posts( $args ); ?>

<?php if (have_posts()) : ?>
  <div class="section videos">
    <ul class="slides">

      <?php while ( have_posts() ) : the_post();

        $name = get_the_title();
        $permalink =  get_the_permalink();

        echo '<li class="home-page-videos">';
        if ( has_post_thumbnail() ) {
          $feat_image_url = wp_get_attachment_url( get_post_thumbnail_id() );
          echo '<a href="' . $permalink . '" class="video-thumbnail" style="background-image:url('.$feat_image_url.');"></a>';
        };
        echo '<h5>' . $name . '</h5>';
        echo '</li>';

      endwhile;?>
    </ul>
  </div>

<?php endif; ?>

<?php get_footer(); ?>
