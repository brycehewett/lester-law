<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article class="single-column-container">

		<header>
			<?php if (function_exists('qt_custom_breadcrumbs')) qt_custom_breadcrumbs();?>
    	<h1><?php the_title(); ?></h1>
    	<?php $title = 'member_title'; echo '<p>' . get_post_meta($post->ID, $title, true) . '</p>';?>
  	</header>

  	<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); }
			
		the_content(); ?>

		<footer>
						
			</div><!--#post-author-->
		</footer>
	</article>
	
	<?php endwhile; endif; ?>
	
<?php get_footer(); ?>