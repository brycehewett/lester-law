<form action="<?php bloginfo('siteurl'); ?>" class="searchform" method="get">
  <div class="input-group search-input">
    <label for="s" class="screen-reader-text hidden"><?php _e('Search for:','lesterlaw'); ?></label>
    <input type="search" class="form-control s" name="s" value="" />
    <button type="submit" class="input-group-addon button red" id="searchsubmit" /><i class="fa fa-search"></i></button>
  </div>
</form>
