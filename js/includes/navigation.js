//Responsive menu toggle
$('.menu-toggle').click(function(){
  $('.menu-toggle, .responsive-menu').toggleClass('active');
  $('.site-wrapper, .top-nav').toggleClass('pushed');
});

//Language Selector Menu Toggle
$('.language-selector').click(function(){
  $(this).toggleClass('menu-open');
});

$('html').click(function() {
  if ($('.language-selector').hasClass('menu-open')) {
    $('.language-selector').toggleClass('menu-open');
  }
});

$('.language-selector').click(function(event){
    event.stopPropagation();
});
