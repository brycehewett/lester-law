//=require ../libs/flexslider/jquery.flexslider.js
(function($){
  $(window).load(function() {
    $('.videos').flexslider({
      animation: "slide",
      itemWidth: 210,
      itemMargin: 5
    });
  });
  $(document).ready(function (){
    //=require includes/navigation.js
  });
})(jQuery);
