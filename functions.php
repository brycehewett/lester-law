<?php

	// Add RSS links to <head> section
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Proper way to enqueue scripts and styles
	 */
	function js_scripts() {
		//Styles
		wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css' );
		//Scripts in the header
		wp_register_script('jQuery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"),false, false);
		wp_enqueue_script( 'jquery-functions', get_template_directory_uri() . '/js/functions.min.js', array('jQuery'), '1.0', true );
	}


	add_action( 'wp_enqueue_scripts', 'js_scripts' );
	// Clean up the <head>
	function removeHeadLinks() {
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wlwmanifest_link');
	}
	add_action('init', 'removeHeadLinks');
	remove_action('wp_head', 'wp_generator');

	function filter_ptags_on_images($content){
		return preg_replace('/<p>\s*(<a .*>)?\s*(<img .* \/>)\s*(<\/a>)?\s*<\/p>/iU', '\1\2\3', $content);
	}

	add_filter('the_content', 'filter_ptags_on_images');

	/*  Add responsive container to embeds
	/* ------------------------------------ */
	function change_video_embed_html( $html ) {
	    return '<div class="video-container">' . $html . '</div>';
	}

	add_filter( 'embed_oembed_html', 'change_video_embed_html', 10, 3 );

	//Register Widget areas
	if (function_exists('register_sidebar')) {
		register_sidebar(array(
			'name' => __('Footer Column 1','lesterlaw' ),
			'id'   => 'footer-col-1',
			'description'   => __( 'Widget area for the first column in the footer.','html5reset' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
		));
		register_sidebar(array(
			'name' => __('Footer Column 2','lesterlaw' ),
			'id'   => 'footer-col-2',
			'description'   => __( 'Widget area for the second column in the footer.','html5reset' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
		));
		register_sidebar(array(
			'name' => __('Footer Column 3','lesterlaw' ),
			'id'   => 'footer-col-3',
			'description'   => __( 'Widget area for the third column in the footer.','html5reset' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
		));
		register_sidebar(array(
			'name' => __('Footer Column 4','lesterlaw' ),
			'id'   => 'footer-col-4',
			'description'   => __( 'Widget area for the fourth column in the footer.','html5reset' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
		));
		register_sidebar(array(
			'name' => __('Right Sidebar','lesterlaw' ),
			'id'   => 'right-sidebar',
			'description'   => __( 'Widget area for the right sidebar on pages.','html5reset' ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h3>',
			'after_title'   => '</h3>'
		));
	}

	//Custom Post Type for Team Members
	add_action( 'init', 'create_posttype' );
	function create_posttype() {
		register_post_type( 'team-members',
			array(
				'labels' => array(
					'name' => __( 'Our Team' ),
					'singular_name' => __( 'Team Member' ),
					'add_new_item' => __( 'Add New Team Member' ),
					'edit_item' => __( 'Edit Team Member' ),
					'not_found' => __( 'No team members found' ),
					'not_found_in_trash' => __( 'No team members found in trash' ),
					'view_item' => __( 'View Page' )

				),
				'public' => true,
				'has_archive' => false,
				'rewrite' => array('slug' => 'our-team'),
				'menu_position' => 20,
				'menu_icon' => 'dashicons-groups',
				'supports' => array(
					'title',
					'editor',
					'thumbnail',
					'page-attributes',
					'custom-fields'
					)
			)
		);
		register_post_type( 'videos',
			array(
				'labels' => array(
					'name' => __( 'Videos' ),
					'singular_name' => __( 'Video' ),
					'add_new_item' => __( 'Add New Video' ),
					'edit_item' => __( 'Edit Video' ),
					'not_found' => __( 'No videos found' ),
					'not_found_in_trash' => __( 'No videos found in trash' ),
					'view_item' => __( 'View Video' )
				),
				'public' => true,
				'has_archive' => false,
				'rewrite' => array('slug' => 'videos'),
				'menu_position' => 21,
				'menu_icon' => 'dashicons-video-alt3',
				'supports' => array(
					'title',
					'editor',
					'thumbnail',
					'page-attributes',
					)
			)
		);
	}

	function register_menus() {
  	register_nav_menus(
	    array(
	      'header-menu' => __( 'Header Menu' ),
	      'footer-menu' => __( 'Footer Menu' ),
				'copyright-menu' => __( 'Copyright Menu' )
	    )
  	);
	}
	add_action( 'init', 'register_menus' );

	add_theme_support( 'post-thumbnails' );

	function custom_excerpt_length($length) {
		return 150;
	}
	function custom_excerpt_more($more) {
		return ' [...]';
	}

	function custom_excerpt($text) {
		global $post;
		$pattern = get_shortcode_regex();

		if ( has_post_thumbnail() ) {
			$permalink = get_the_permalink();
			$title = get_the_title();
			$thumbnail = get_the_post_thumbnail(null, 'post-thumbnail','');

	    $text = '<a class="teaser-image" href="'.$permalink.'" alt="'.$title.'">'.$thumbnail.'</a>';
	  }

		preg_match_all( '/'. $pattern .'/s', $post->post_content, $matches );
		if (is_array( $matches ) && array_key_exists(2, $matches )) {
	    //I'm currently using only youtube and flv videos. If you use
	    //other videos then add those shortcodes in the following array
			$arr=array('youtube','flv');
			foreach($matches[2] as $v)
			{
				if(in_array($v, $matches[2]))
				{
					$video = apply_filters('the_content', $matches[0][0]);
					$text = $video;
					$text .= '<p>' . get_the_excerpt() . '</p>';
					$text = apply_filters('wptexturize', $text);
					$text= apply_filters('convert_chars', $text);
				}
			}
		}
		return $text;
	}
	add_filter('the_excerpt', 'custom_excerpt');
	add_filter('excerpt_more', 'custom_excerpt_more');
	add_filter( 'excerpt_length', 'custom_excerpt_length');

	/**
	 * Improves the caption shortcode with HTML5 figure & figcaption; microdata & wai-aria attributes
	 *
	 * @param  string $val     Empty
	 * @param  array  $attr    Shortcode attributes
	 * @param  string $content Shortcode content
	 * @return string          Shortcode output
	 */
	function jk_img_caption_shortcode_filter($val, $attr, $content = null)
	{
		extract(shortcode_atts(array(
			'id'      => '',
			'align'   => 'aligncenter',
			'width'   => '',
			'caption' => ''
		), $attr));

		// No caption, no dice... But why width?
		if ( 1 > (int) $width || empty($caption) )
			return $val;

		if ( $id )
			$id = esc_attr( $id );

		// Add itemprop="contentURL" to image - Ugly hack
		$content = str_replace('<img', '<img itemprop="contentURL"', $content);

		return '<figure id="' . $id . '" aria-describedby="figcaption_' . $id . '" class="wp-caption ' . esc_attr($align) . '" itemscope itemtype="http://schema.org/ImageObject" style="width: ' . (0 + (int) $width) . 'px">' . do_shortcode( $content ) . '<figcaption id="figcaption_'. $id . '" class="wp-caption-text" itemprop="description">' . $caption . '</figcaption></figure>';
	}
	add_filter( 'img_caption_shortcode', 'jk_img_caption_shortcode_filter', 10, 3 );
	/**
	 * Optional: set 'ot_show_pages' filter to false.
	 * This will hide the settings & documentation pages.
	 */
	add_filter( 'ot_show_pages', '__return_false' );
	/**
	 * Required: set 'ot_theme_mode' filter to true.
	 */
	add_filter( 'ot_theme_mode', '__return_true' );
	/**
	 * Required: include OptionTree.
	 */
	include_once( 'libs/option-tree/ot-loader.php' );
	/**
	 * Theme Options
	 */
	include_once( 'includes/_theme-options.php' );
	/**
	* Breadcrumbs
	*/
	include_once( 'includes/breadcrumb.php' );
	/**
	* Exlude pages from search
	*/
	function SearchFilter($query) {
		if ($query->is_search) {
			$query->set('post_type', 'post');
		}
		return $query;
	}

	add_filter('pre_get_posts','SearchFilter');
?>
