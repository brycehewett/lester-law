<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post();
  include('includes/page-header.php') ?>


    <article class="single-column-container">
      <?php if (function_exists('qt_custom_breadcrumbs')) qt_custom_breadcrumbs();?>
      
      <header>
        <h1><?php the_title(); ?></h1>
      
      </header>

  		<?php the_content(); ?>

  	</article>

		
	<?php endwhile; endif; ?>

<?php get_footer(); ?>

