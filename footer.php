      <?php include('includes/footer-social-links.php') ?>

      <footer class="footer container" class="source-org vcard copyright">
        <?php $defaults = array(
          'theme_location'  => 'footer-menu',
          'container'       => '',
          'fallback_cb'     => false,
          'items_wrap'      => '<nav class="footer-menu"><ul>%3$s</ul></nav>',
          'depth'           => 1
        ); wp_nav_menu( $defaults ); ?>

        <div class="footer-static-col">
          <a href="#" class="footer-logo"></a>
          <div class="footer-social-icons">
            <a href="https://twitter.com/LesterLawTN" class="twitter-follow-button" data-size="large" data-show-screen-name="false" data-show-count="false">Follow @LesterLawTN</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script><br />

            <script src="https://apis.google.com/js/platform.js"></script>
            <div class="g-ytsubscribe" data-channelid="UCqve-BBOz7F7gEeMs6tk_Pg" data-layout="default" data-count="default"></div><br />

            <div class="fb-like" data-href="https://www.facebook.com/MartinWLesterPA" data-layout="button_count" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
          </div>
        </div>
        <div class="footer-custom-content">
          <div class="footer-col-1"><?php dynamic_sidebar('footer-col-1')?></div>
          <div class="footer-col-2"><?php dynamic_sidebar('footer-col-2')?></div>
          <div class="footer-col-3"><?php dynamic_sidebar('footer-col-3')?></div>
          <div class="footer-col-4"><?php dynamic_sidebar('footer-col-4')?></div>
        </div>
        <div class="copyright">
          <p>&copy;<?php echo date("Y"); echo " "; bloginfo('name'); ?></p>
          <?php $secondary_footer_defaults = array(
            'theme_location'  => 'copyright-menu',
            'container'       => 'ul',
            'before'          => ' &#9679; ',
            'fallback_cb'     => false,
            'items_wrap'      => '<ul>%3$s</ul>',
            'depth'           => 1
          ); wp_nav_menu( $secondary_footer_defaults ); ?>
        </div>
      </footer>

        <?php wp_footer(); ?>

        <?php
          if ( function_exists( 'ot_get_option' ) ) {
            $google_analytics = ot_get_option( 'google_analytics' );

          if ( ! empty( $google_analytics ) ) {


        echo "<script>
                (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
                })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

                ga('create', '".$google_analytics."', 'auto');
                ga('send', 'pageview');

              </script>";

            }

        }?>
      </div><!--.content-->
    </div><!--.site-wrapper-->
  </body>
</html>
