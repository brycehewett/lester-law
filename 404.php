<?php get_header(); ?>
  <section>
    <header>
      <h1><?php _e( 'Page Not Found', 'lesterlaw' ); ?></h1>
      <p><?php _e( 'Error 404', 'lesterlaw' ); ?></p>
    </header>
  </section>
<?php get_footer(); ?>
