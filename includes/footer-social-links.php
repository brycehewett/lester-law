<div class="footer-bar container">
  <h2><?php _e( 'Follow Us On Social Media', 'lesterlaw' ); ?></h2>
  <span class="footer-bar__social-links">
    <a href="https://www.facebook.com/MartinWLesterPA/" target="_blank"><i class="footer-bar__social-button fa fa-facebook" aria-hidden="true"></i></a>
    <a href="https://www.youtube.com/lesterlaw" target="_blank"><i class="footer-bar__social-button fa fa-youtube-play" aria-hidden="true"></i></a>
    <a href="https://twitter.com/LesterLawTN" target="_blank"><i class="footer-bar__social-button fa fa-twitter" aria-hidden="true"></i></a>
  </span>
</div>
