<?php
/**
 * Initialize the custom theme options.
 */
add_action( 'init', 'custom_theme_options' );

/**
 * Build the custom settings & update OptionTree.
 */
function custom_theme_options() {

  /* OptionTree is not loaded yet, or this is not an admin request */
  if ( ! function_exists( 'ot_settings_id' ) || ! is_admin() )
    return false;

  /**
   * Get a copy of the saved settings array.
   */
  $saved_settings = get_option( ot_settings_id(), array() );

  /**
   * Custom settings array that will eventually be
   * passes to the OptionTree Settings API Class.
   */
  $custom_settings = array(
    'contextual_help' => array(
      'sidebar'       => ''
    ),
    'sections'        => array(
      array(
        'id'          => 'general',
        'title'       => 'General'
      ),
      array(
        'id'          => 'social',
        'title'       => 'Social'
      )
    ),
    'settings'        => array(
      array(
        'id'          => 'google_analytics',
        'label'       => 'Google Analytics',
        'desc'        => 'Enter your Google Analytics UA code here.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'yes',
            'label'       => 'Yes',
            'src'         => ''
          )
        )
      ),
      array(
        'id'          => 'favicon',
        'label'       => 'Favicon',
        'desc'        => 'Favicon size should be 16x16px. Make sure to use an image with .ico extension.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'yes',
            'label'       => 'Yes',
            'src'         => ''
          )
        )
      ),
      array(
        'id'          => 'apple_touch_icon',
        'label'       => 'Apple Touch Icon',
        'desc'        => '57x57 for older iPhones, 72x72 for iPads, 114x114 for the iPhone4 retina display. Just go ahead and use the biggest one. Transparency is not recommended',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'general',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'yes',
            'label'       => 'Yes',
            'src'         => ''
          )
        )
      ),
      array(
        'id'          => 'facebook_app_id',
        'label'       => 'Facebook App ID',
        'desc'        => 'Enter your facebook app ID here.',
        'std'         => '',
        'type'        => 'text',
        'section'     => 'social',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'yes',
            'label'       => 'Yes',
            'src'         => ''
          )
        )
      ),
      array(
        'id'          => 'default_facebook_logo',
        'label'       => 'Default Facebook Logo',
        'desc'        => 'Upload the logo that will be used in case there is no image on the page for facebook to use.',
        'std'         => '',
        'type'        => 'upload',
        'section'     => 'social',
        'rows'        => '',
        'post_type'   => '',
        'taxonomy'    => '',
        'min_max_step'=> '',
        'class'       => '',
        'condition'   => '',
        'operator'    => 'and',
        'choices'     => array(
          array(
            'value'       => 'yes',
            'label'       => 'Yes',
            'src'         => ''
          )
        )
      )
    )
  );

  /* allow settings to be filtered before saving */
  $custom_settings = apply_filters( ot_settings_id() . '_args', $custom_settings );

  /* settings are not the same update the DB */
  if ( $saved_settings !== $custom_settings ) {
    update_option( ot_settings_id(), $custom_settings );
  }

  /* Lets OptionTree know the UI Builder is being overridden */
  global $ot_has_custom_theme_options;
  $ot_has_custom_theme_options = true;

}
