<?php if (function_exists('pll_home_url')){
  $home_url = pll_home_url();
} else {
  $home_url = get_option('home');
}?>

<header class="top-nav">
  <div class="secondary-header">

    <span class="contact-info"><a href="tel:423-402-0608">423.402.0608</a> | <a href="mailto:info@lesterlaw.org">info@lesterlaw.org</a></span>

    <?php if (function_exists('pll_home_url')){?>
      <span class="language-selector">
        <i class="fa fa-globe" aria-hidden="true"></i>&nbsp;&nbsp;<?php echo pll_current_language('name');?>&nbsp;&nbsp;<i class="fa fa-caret-down" aria-hidden="true"></i>

        <ul class="language-selector__menu"><?php pll_the_languages();?></ul>
      </span>
    <?php } ?>
  </div>
  <div class="primary-header">
    <a class="logo" href="<?php echo $home_url ?>" alt="<?php bloginfo('name'); ?>">
      <img src="<?php echo get_template_directory_uri() . '/images/logo.png';?>"/>
    </a>
    <div class="menu-toggle"></div>

    <?php $defaults = array(
      'theme_location'  => 'header-menu',
      'container'       => '',
      'fallback_cb'     => false,
      'items_wrap'      => '<nav class="header-menu"><ul>%3$s</ul></nav>',
      'depth'           => 2
    ); wp_nav_menu( $defaults ); ?>
  </div>
</header>
