<nav class="responsive-menu">
  <?php $defaults = array(
    'theme_location'  => 'header-menu',
    'container'       => '',
    'fallback_cb'     => false,
    'items_wrap'      => '<ul>%3$s</ul>',
    'depth'           => 2,);
  wp_nav_menu( $defaults ); ?>

	<div class="menu-social-icons">
    <a href="https://twitter.com/LesterLawTN" class="twitter-follow-button" data-size="large" data-show-screen-name="false" data-show-count="false">Follow @LesterLawTN</a><script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script><br />

    <script src="https://apis.google.com/js/platform.js"></script>
    <div class="g-ytsubscribe" data-channelid="UCqve-BBOz7F7gEeMs6tk_Pg" data-layout="default" data-count="default"></div><br />

    <div class="fb-like" data-href="https://www.facebook.com/MartinWLesterPA" data-layout="button_count" data-action="like" data-size="large" data-show-faces="false" data-share="false"></div>
	</div>
</nav>
