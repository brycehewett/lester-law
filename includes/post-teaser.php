<article class="post-teaser">

  <header>
    <h2><a href="<?php the_permalink() ?>"><?php the_title(); ?></a></h2>
    <?php include (TEMPLATEPATH . '/includes/meta.php' ); ?>
  </header>

  <?php the_excerpt(); ?>

</article>
