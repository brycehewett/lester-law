<div class="post-nav">
	<div class="prev-posts">
		<?php if (is_single()) {
			previous_post_link('%link', '&laquo; ' . __('Previous Post', 'lesterlaw'));
		} else {
			next_posts_link('&laquo; ' . __('Older Posts', 'lesterlaw'));
		}?>
	</div><!--.older-->
	<div class="next-posts">
		<?php if (is_single()) {
			next_post_link('%link', __('Next Post', 'lesterlaw') . ' &raquo;');
		} else {
			previous_posts_link(__('Newer Posts', 'lesterlaw') . ' &raquo;');
		} ?>
	</div><!--.older-->
</div>
