<?php get_header(); ?>

	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

	<article class="single-column-container">

		<header>
			<?php if (function_exists('qt_custom_breadcrumbs')) qt_custom_breadcrumbs();?>
    	<h1><?php the_title(); ?></h1>
  	</header>

  	<?php the_content(); ?>

		<footer>

			</div><!--#post-author-->
		</footer>
	</article>

	<?php endwhile; endif; ?>

<?php get_footer(); ?>
