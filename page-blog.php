<?php
/*
Template Name: Blog Template
*/
?>
<?php get_header(); ?>

<div class="two-column-container">
  <div class="column-two-thirds">

    <?php if (function_exists('qt_custom_breadcrumbs')) qt_custom_breadcrumbs();?>
    <h1><?php _e('Recent Articles','lesterlaw'); ?></h1>

    <?php query_posts('posts_per_page='.get_option('posts_per_page').'&paged='. get_query_var('paged')); ?>

    <?php if( have_posts() ): ?>

          <?php while( have_posts() ): the_post(); ?>

          <?php include('includes/post-teaser.php');?>

          <?php endwhile; ?>

    <?php include('includes/post-nav.php'); ?>

    <?php else: ?>

      <div class="post-404 noposts">

          <p><?php _e('No Posts Found','lesterlaw'); ?></p>

        </div><!-- /.post-404 -->

    <?php endif; wp_reset_query(); ?>

  </div><!-- /.column-two-thirds -->

  <?php get_sidebar()?>

</div><!--/.tow-column-container-->

<?php get_footer(); ?>
